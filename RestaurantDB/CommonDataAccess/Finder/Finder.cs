﻿using System.Linq.Expressions;
using CommonDataAccess.Finder.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace CommonDataAccess.Finder
{
    public class Finder<T> : IFinder<T>
        where T : class
    {
        private readonly DbContext _context;

        public Finder(DbContext context)
        {
            _context = context;
        }

        public IQueryable<T> Find()
        {
            return _context.Set<T>();
        }

        public Task<T?> Find(int id)
        {
            return _context.FindAsync<T>(id).AsTask();
        }

        public Task<bool> ExistsAsync(Expression<Func<T, bool>> condition)
        {
            return _context.Set<T>().AnyAsync(condition);
        }

        public Task<int> CountAsync()
        {
            return _context.Set<T>().CountAsync();
        }
    }
}