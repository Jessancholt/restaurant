﻿using System.Linq.Expressions;

namespace CommonDataAccess.Finder.Interfaces
{
    public interface IFinder<T>
        where T : class
    {
        public IQueryable<T> Find();

        public Task<T?> Find(int id);

        public Task<bool> ExistsAsync(Expression<Func<T, bool>> condition);

        public Task<int> CountAsync();
    }
}