﻿namespace CommonDataAccess.Repository.Interfaces
{
    public interface IRepository<T>
        where T : class
    {
        public Task CreateAsync(T entity);

        public T UpdateAsync(T entity);

        public T DeleteAsync(T entity);
    }
}