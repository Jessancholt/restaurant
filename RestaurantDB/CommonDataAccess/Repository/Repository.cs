﻿using CommonDataAccess.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace CommonDataAccess.Repository
{
    public class Repository<T> : IRepository<T>
        where T : class
    {
        private readonly DbContext _context;

        public Repository(DbContext context)
        {
            _context = context;
        }

        public Task CreateAsync(T entity)
        {
            return _context.AddAsync(entity).AsTask();
        }

        public T UpdateAsync(T entity)
        {
            return _context.Update(entity).Entity;
        }

        public T DeleteAsync(T entity)
        {
            return _context.Remove(entity).Entity;
        }
    }
}