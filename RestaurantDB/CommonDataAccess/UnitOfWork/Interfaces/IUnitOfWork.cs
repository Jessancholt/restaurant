﻿namespace CommonDataAccess.UnitOfWork.Interfaces
{
    public interface IUnitOfWork
    {
        public Task SaveChangesAsync();
    }
}