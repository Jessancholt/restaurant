﻿using Microsoft.AspNetCore.Mvc;
using Restaurant.Models;
using Restaurant.Models.Home;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Restaurant.Components
{
    public class BasketInfoViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            Cart cart = HttpContext.Session.Get<Cart>("Cart");
            return View(cart);
        }
    }
}
