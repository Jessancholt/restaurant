﻿using System;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Restaurant.Models.Account;
using RestaurantDB.Model;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using RestaurantBL.Services.Interfaces;
using RestaurantDB.Constants;

namespace Restaurant.Controllers
{
    public class AccountController : Controller
    {
        private readonly IService<User> _userService;
        private readonly IService<Role> _roleService;
        public AccountController(IService<User> userService,
                                 IService<Role> roleService)
        {
            _userService = userService;
            _roleService = roleService;
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userService.FirstOrDefaultAsync(x =>
                    x.Login == model.Login && x.Password == model.Password);
                if (user != null)
                {
                    await Authenticate(user);

                    return RedirectToAction("GetAll", "Catalog");
                }

                ModelState.AddModelError("", "Некорректные логин и(или) пароль");
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterModel model)
        {
            var regex = @"\+[0-9]{12}";
            Match match = Regex.Match(model.PhoneNumber, regex, RegexOptions.IgnoreCase);
            if (match.Success)
            {
                // добавляем пользователя в бд 
                if (ModelState.IsValid)
                {
                    var isExists = await _userService.ExistsAsync(x => x.Login == model.Login);
                    if (!isExists)
                    {
                        var user = new User()
                        {
                            Login = model.Login,
                            Password = model.Password,
                            FirstName = model.FirstName,
                            LastName = model.LastName,
                            PhoneNumber = model.PhoneNumber
                        };
                        var isInRole = model.Role != null;

                        if (!isInRole)
                        {
                            user.Role = await _roleService.FirstOrDefaultAsync(r =>
                                r.Name == (RoleName) Enum.Parse(typeof(RoleName), "User", true));
                        }
                        else
                        {
                            user.Role = await _roleService.FirstOrDefaultAsync(r =>
                                r.Name == (RoleName) Enum.Parse(typeof(RoleName), model.Role, true));
                        }

                        // добавляем пользователя в бд
                        await _userService.CreateAsync(user);


                        await Authenticate(user); // аутентификация

                        return RedirectToAction("GetAll", "Catalog");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Пользователь существует!");
                    }
                }

                return View(model);
            }
            ModelState.AddModelError("", "Номер неправильного формата!");
            return View(model);
            //}
        }

        private async Task Authenticate(User user)
        {
            var role = await _roleService.FirstOrDefaultAsync(x => x.Id == user.RoleId);
            // создаем один claim
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Id.ToString()),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, role.Name.ToString())
            };
            // создаем объект ClaimsIdentity
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            // установка аутентификационных куки
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("GetAll", "Catalog");
        }
    }
}
