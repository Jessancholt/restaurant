﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Restaurant.Models.Home;
using Restaurant.Models;
using Restaurant.Models.ViewModels;
using System.Threading.Tasks;
using RestaurantBL.Services.Interfaces;
using RestaurantDB.Constants;
using RestaurantDB.Model;
using CartLine = RestaurantDB.Model.CartLine;

namespace Restaurant.Controllers
{
    public class BasketController : Controller
    {
        private IService<Dessert> _dessertService;
        private IService<Order> _orderService;
        private IService<CartLine> _cartLineService;
        private IService<User> _userService;

        public BasketController(IService<Dessert> dessertService, IService<Order> orderService, IService<CartLine> cartLineService, IService<User> userService)
        {
            _dessertService = dessertService;
            _orderService = orderService;
            _cartLineService = cartLineService;
            _userService = userService;
        }

        public IActionResult Basket()
        {
            return View(new CartIndexViewModel
            {
                Cart = GetCart()
            });
        }

        [HttpPost]
        public async Task<IActionResult> AddToCart(int dishId)
        {
            var dish = await _dessertService
                .FirstOrDefaultAsync(g => g.Id == dishId);

            if (dish != null)
            {
                Cart cart = HttpContext.Session.Get<Cart>("Cart");
                if (cart == null)
                {
                    cart = new Cart();
                }
                cart.AddItem(dish, 1);
                HttpContext.Session.Set("Cart", cart);

                //////
                //var cartLineExists = await _cartLineService.ExistsAsync(x => x.DessertId == dish.Id);

                //if (!cartLineExists)
                //{
                //    var cartLine = new CartLine() { DessertId = dish.Id, Quantity = 1 };
                //    await _cartLineService.CreateAsync(cartLine);
                //}
                //else
                //{
                //    var cartLine = await _cartLineService.FirstOrDefaultAsync(x => x.DessertId == dish.Id);
                //    cartLine.Quantity += 1;
                //    await _cartLineService.UpdateAsync(cartLine);
                //}
                //////

            }


            return ViewComponent("BasketInfo");
        }

        public async Task<IActionResult> RemoveFromCart(int dishId)
        {
            var dish = await _dessertService
                .FirstOrDefaultAsync(g => g.Id == dishId);

            if (dish != null)
            {
                Cart cart = HttpContext.Session.Get<Cart>("Cart");
                cart.RemoveLine(dish);
                HttpContext.Session.Set("Cart", cart);
                var cartLine = await _cartLineService.FirstOrDefaultAsync(x => x.DessertId == dish.Id);
                await _cartLineService.DeleteAsync(cartLine.Id);
            }
            return RedirectToAction("Basket", "Basket");
        }

        public IActionResult Summary(Cart cart)
        {
            return PartialView(cart);
        }

        [HttpPost]
        public async Task<IActionResult> Checkout()
        {
            var cart = HttpContext.Session.Get<Cart>("Cart");
            var now = DateTimeOffset.UtcNow;
            var login = User.Identity.Name;
            var user = await _userService.FirstOrDefaultAsync(u => u.Id == Int32.Parse(login));
            var order = new Order()
            {
                Status = OrderStatus.Active,
                StartDate = now,
                EndDate = now + TimeSpan.FromMinutes(30),
                UserId = user.Id
            };
            //foreach (var line in cart.Lines)
            //{
            //    order.CartLines.Add(await _cartLineService.FirstOrDefaultAsync(d => d.DessertId == line.DishId));
            //}
            await _orderService.CreateAsync(order);
            return Json(new { success = "Заказ успешно добавлен!" });
        }

        public Cart GetCart()
        {
            Cart cart = HttpContext.Session.Get<Cart>("Cart");
            if (cart == null)
            {
                cart = new Cart();
                HttpContext.Session.Set("Cart", cart);
            }
            return cart;
        }
    }
}
