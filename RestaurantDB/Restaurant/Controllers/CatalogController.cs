﻿using Microsoft.AspNetCore.Mvc;
using Restaurant.Models.Home;
using Restaurant.Models.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Converters;
using RestaurantBL.Services.Interfaces;
using RestaurantDB.Model;

namespace Restaurant.Controllers
{
    public class CatalogController : Controller
    {
        private readonly IService<Catalog> _catalogService;
        public CatalogController(IService<Catalog> catalogService)
        {
            _catalogService = catalogService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll(string title)
        {
            var menus = await _catalogService.GetAsync();
            var model = menus
                .Select(u => new CatalogInfo
                {
                    Id = u.Id,
                    Title = u.Title,
                    Dishes = u.Dishes
                });

            IndexViewModel viewModel = new IndexViewModel
            {
                Menus = model
            };

            return View(viewModel);
        }

        [HttpGet]
        public async Task<IActionResult> GetInfo(int id)
        {
            var task = await _catalogService.GetAsync(id);
            var model = new CatalogInfo
            {
                Id = task.Id,
                Title = task.Title,
                Dishes = task.Dishes
            };

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> CatalogInfo(CatalogInfo info)
        {
            var menu = await _catalogService.GetAsync(info.Id);
            menu.Title = info.Title;


             await _catalogService.UpdateAsync(menu);
            return Json(new { success = "Успешно" });
        }

        [HttpPost]
        public async Task<IActionResult> CreateCatalog(CatalogInfo info)
        {
            var isExists = await _catalogService.ExistsAsync(x => x.Title == info.Title);
            if (!isExists)
            {
                var menu = new Catalog()
                {
                    Title = info.Title,
                    Dishes = (ICollection<Dessert>)info.Dishes
                };

                await _catalogService.CreateAsync(menu);
                return Redirect(Url.Action("GetAll", "Catalog"));
            }

            ModelState.AddModelError("", "Каталог существует!");
            return View("CreatePage");
        }

        public IActionResult CreatePage()
        {
            return View();
        }

        public async Task<IActionResult> DeleteCatalog(CatalogInfo info)
        {
            await _catalogService.DeleteAsync(info.Id);

            return RedirectToAction("GetAll", "Catalog");
        }
    }
}