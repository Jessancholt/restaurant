﻿using Microsoft.AspNetCore.Mvc;
using Restaurant.Models.Home;
using Restaurant.Models.ViewModels;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using RestaurantBL.Services.Interfaces;
using RestaurantDB.Model;

namespace Restaurant.Controllers
{
    public class DessertController : Controller
    {
        private readonly IService<Dessert> _dessertService;
        private readonly IService<Catalog> _catalogService;
        private readonly IService<User> _userService;

        public DessertController(IService<Dessert> dessertService, IService<Catalog> catalogService, IService<User> userService)
        {
            _dessertService = dessertService;
            _catalogService = catalogService;
            _userService = userService;
        }

        [HttpGet]
        public async Task<IActionResult> GetInfo(int id)
        {
            var dish = await _dessertService.GetAsync(id);
            var model = new DishInfo
            {
                Id = dish.Id,
                Title = dish.Title,
                Description = dish.Description,
                Weight = dish.Weight,
                Price = dish.Price
            };

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> GetAll(int id, string title)
        {
            var menu = await _catalogService.FirstOrDefaultAsync(m => m.Id == id);
            var dishes = menu.Dishes;
            if (dishes != null)
            {
                var model = dishes
                   .Select(u => new DishInfo
                   {
                       Id = u.Id,
                       Title = u.Title,
                       Description = u.Description,
                       Weight = u.Weight,
                       Price = u.Price,
                       Avatar = u.Image,
                       Rate = u.Rate
                   });

                IQueryable<DishInfo> source = model.AsQueryable();
                //фильтрация
                if (!String.IsNullOrEmpty(title))
                {
                    source = source.Where(p => p.Title.Contains(title));
                }

                IndexViewModel viewModel = new IndexViewModel
                {
                    FilterViewModel = new FilterViewModel(title),
                    Dishes = source,
                    MenuId = id
                };

                return PartialView(viewModel);

            }

            return View();
        }


        [HttpPost]
        public async Task<IActionResult> DishInfo(DishInfo info)
        {
            var dish = await _dessertService.GetAsync(info.Id);
            dish.Title = info.Title;
            dish.Description = info.Description;


            await _dessertService.UpdateAsync(dish);
            return Json(new { success = "Успешно" });
        }

        [HttpPost]
        public async Task<IActionResult> CreateDish(DishInfo info)
        {
            var isExisted = await _dessertService.ExistsAsync(x => x.Title == info.Title);
            if (!isExisted)
            {
                var menu = await _catalogService.FirstOrDefaultAsync(m => m.Id == info.MenuId);
                var dish = new Dessert()
                {
                    Price = info.Price,
                    Weight = info.Weight,
                    Title = info.Title,
                    Description = info.Description,
                };

                byte[] imageData = null;
                using (var binaryReader = new BinaryReader(info.Image.OpenReadStream()))
                {
                    imageData = binaryReader.ReadBytes((int)info.Image.Length);
                }

                dish.Image = imageData;

                menu.Dishes.Add(dish);

                await _dessertService.CreateAsync(dish);
                return Redirect(Url.Action("GetAll", "Catalog"));
            }

            ModelState.AddModelError("", "Десерт существует!");
            return View("CreatePage");
        }

        public IActionResult CreatePage(DishInfo info)
        {
            return View(info);
        }

        public async Task<IActionResult> DeleteDish(DishInfo info)
        {
            await _dessertService.DeleteAsync(info.Id);

            return RedirectToAction("GetAll", "Dessert", new { id = info.MenuId });
        }

        [HttpPost]
        public async Task<IActionResult> AddToFavourites(int dishId)
        {
            var login = User.Identity.Name;
            var user = await _userService.FirstOrDefaultAsync(u => u.Id == Int32.Parse(login));
            var dish = await _dessertService.FirstOrDefaultAsync(u => u.Id == dishId);
            user.ChosenDishes.Add(dish);
            await _dessertService.UpdateAsync(dish);
            //return Redirect(Url.Action("GetAll", "Menu"));
            return Json(new { });
        }

        [HttpGet]
        public async Task<IActionResult> Favourites()
        {
            var login = User.Identity.Name;
            var user = await _userService.FirstOrDefaultAsync(u => u.Id == Int32.Parse(login));
            var dishes = user.ChosenDishes;
            var model = dishes
                .Select(u => new DishInfo
                {
                    Id = u.Id,
                    Title = u.Title,
                    Description = u.Description,
                    Weight = u.Weight,
                    Price = u.Price,
                    Avatar = u.Image,
                    Rate = u.Rate
                });
            IndexViewModel viewModel = new IndexViewModel
            {
                Dishes = model
            };
            return View(viewModel);
        }
    }
}
