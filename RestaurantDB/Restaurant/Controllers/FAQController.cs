﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Restaurant.Models.Home;
using Restaurant.Models.ViewModels;
using RestaurantBL.Services.Interfaces;
using RestaurantDB.Model;

namespace Restaurant.Controllers
{
    public class FAQController : Controller
    {
        private readonly IService<FAQ> _faqService;

        public FAQController(IService<FAQ> faqService)
        {
            _faqService = faqService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var entity = await _faqService.GetAsync();
            var model = entity.Select(x => new FAQInfo()
            {
                Answer = x.Answer,
                Question = x.Question
            });
            IndexViewModel viewModel = new IndexViewModel
            {
                Faqs = model
            };
            return View(viewModel);
        }

        [HttpGet]
        public async Task<IActionResult> GetInfo(int id)
        {
            var user = await _faqService.GetAsync(id);
            var model = new FAQInfo()
            {
                Id = user.Id,
                Answer = user.Answer,
                Question = user.Question
            };

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> FaqInfo(FAQInfo info)
        {
            var user = await _faqService.GetAsync(info.Id);
            user.Answer = info.Answer;

            await _faqService.UpdateAsync(user);

            return Json(new { success = "Успешно" });
        }

        [HttpPost]
        public async Task<IActionResult> Ask(FAQInfo info)
        {
            var isExists = await _faqService.ExistsAsync(x => x.Question == info.Question);
            if (!isExists)
            {
                var faq = new FAQ()
                {
                    Question = info.Question
                };

                await _faqService.CreateAsync(faq);

                return Redirect(Url.Action("GetAll", "FAQ"));
            }

            ModelState.AddModelError("", "Вопрос существует!");
            return View("AskPage");
        }

        public IActionResult AskPage()
        {
            return View();
        }

        public async Task<IActionResult> DeleteFaq(FAQInfo info)
        {
            await _faqService.DeleteAsync(info.Id);

            return RedirectToAction("GetAll", "FAQ");
        }
    }
}
