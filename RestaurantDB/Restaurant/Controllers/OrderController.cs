﻿using Microsoft.AspNetCore.Mvc;
using Restaurant.Models.Home;
using Restaurant.Models.ViewModels;
using System;
using System.Linq;
using System.Threading.Tasks;
using RestaurantBL.Services.Interfaces;
using RestaurantDB.Constants;
using RestaurantDB.Model;

namespace Restaurant.Controllers
{
    public class OrderController : Controller
    {
        private readonly IService<Order> _orderService;
        public OrderController(IService<Order> orderService)
        {
            _orderService = orderService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var menus = await _orderService.GetAsync();

            var model = menus
                .Select(u => new OrderInfo
                {
                    Id = u.Id,
                    Status = u.Status.ToString(),
                    Dishes = u.CartLines.ToList()
                });

            IndexViewModel viewModel = new IndexViewModel
            {
                Orders = model
            };

            return View(viewModel);
        }

        [HttpGet]
        public async Task<IActionResult> GetInfo(int id)
        {
            var order = await _orderService.GetAsync(id);
            var model = new OrderInfo
            {
                Id = order.Id,
                Status = order.Status.ToString(),
                Dishes = order.CartLines.ToList()
            };

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> OrderInfo(OrderInfo info)
        {
            var menu = await _orderService.GetAsync(info.Id);
            menu.Status = (OrderStatus)Enum.Parse(typeof(OrderStatus), info.Status);

            await _orderService.UpdateAsync(menu);
            return Json(new { success = "Успешно" });
        }

        public async Task<IActionResult> DeleteOrder(OrderInfo info)
        {
            await _orderService.DeleteAsync(info.Id);

            return RedirectToAction("GetAll", "Order");
        }
    }
}
