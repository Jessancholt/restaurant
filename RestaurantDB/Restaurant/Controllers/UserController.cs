﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Restaurant.Models;
using Restaurant.Models.Home;
using Restaurant.Models.ViewModels;
using RestaurantDB.Model;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis.Differencing;
using RestaurantBL.Services.Interfaces;
using RestaurantDB.Constants;

namespace Restaurant.Controllers
{
    public class UserController : Controller
    {
        private readonly IService<User> _userService;
        private readonly IService<Dessert> _dishService;
        private readonly IService<Role> _roleService;
        private readonly string _regex = @"\+[0-9]{12}";

        public UserController(IService<User> userService, IService<Dessert> dessertService, IService<Role> roleService)
        {
            _userService = userService;
            _dishService = dessertService;
            _roleService = roleService;
        }

        [HttpGet]
        public async Task<IActionResult> GetInfo(int id)
        {
            var user = await _userService.GetAsync(id);
            var role = await _roleService.GetAsync(user.RoleId);
            var model = new UserInfo
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Login = user.Login,
                Password = user.Password,
                Role = role.Name.ToString(),
                PhoneNumber = user.PhoneNumber,
                Dishes = user.ChosenDishes.ToList()
            };

            return View(model);
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> GetAll(string firstName, string lastName, int page = 1,
            SortState sortOrder = SortState.IdAsc)
        {
            int pageSize = 3; // количество элементов на странице

            var users = await _userService.GetAsync();

            var model = users
                .Select(u => new UserInfo
                {
                    Id = u.Id,
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    Role = ((RoleName)u.RoleId).ToString()
                });


            IQueryable<UserInfo> source = model.AsQueryable();

            if (!String.IsNullOrEmpty(firstName))
            {
                source = source.Where(p => p.FirstName.Contains(firstName));
            }

            if (!String.IsNullOrEmpty(lastName))
            {
                source = source.Where(p => p.LastName.Contains(lastName));
            }

            // сортировка
            switch (sortOrder)
            {
                case SortState.IdDesc:
                    source = source.OrderByDescending(s => s.Id);
                    break;

                case SortState.FirstNameAsc:
                    source = source.OrderBy(s => s.FirstName);
                    break;

                case SortState.FirstNameDesc:
                    source = source.OrderByDescending(s => s.FirstName);
                    break;

                case SortState.LastNameAsc:
                    source = source.OrderBy(s => s.LastName);
                    break;

                case SortState.LastNameDesc:
                    source = source.OrderByDescending(s => s.LastName);
                    break;

                default:
                    source = source.OrderBy(s => s.Id);
                    break;
            }



            // пагинация
            var count = source.Count();
            var items = source.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            IndexViewModel viewModel = new IndexViewModel
            {
                PageViewModel = new PageViewModel(count, page, pageSize),
                SortViewModel = new SortViewModel(sortOrder),
                FilterViewModel = new FilterViewModel(firstName, lastName),
                Users = items
            };

            return View(viewModel);
        }

        [HttpPost]
        public async Task<IActionResult> UserInfo(UserInfo info)
        {
            Match match = Regex.Match(info.PhoneNumber, _regex, RegexOptions.IgnoreCase);
            if (match.Success)
            {
                var user = await _userService.GetAsync(info.Id);
                user.FirstName = info.FirstName;
                user.LastName = info.LastName;
                user.Login = info.Login;
                user.Password = info.Password;
                user.PhoneNumber = info.PhoneNumber;
                info.Role ??= user.Role.Name.ToString();
                var userRole = await _roleService.FirstOrDefaultAsync(r =>
                    r.Name == (RoleName) Enum.Parse(typeof(RoleName), info.Role, true));
                user.Role = userRole;
                await _userService.UpdateAsync(user);

                return Json(new { success = "Успешно" });
            }
            return Json(new { success = "Номер неправильного формата!" });
        }

        [HttpPost]
        public async Task<IActionResult> CreateUser(UserInfo info)
        {
            Match match = Regex.Match(info.PhoneNumber, _regex, RegexOptions.IgnoreCase);
            if (match.Success)
            {
                var isExists = await _userService.ExistsAsync(x => x.Login == info.Login);
                if (!isExists)
                {
                    var user = new User()
                    {
                        FirstName = info.FirstName,
                        LastName = info.LastName,
                        Login = info.Login,
                        Password = info.Password,
                        PhoneNumber = info.PhoneNumber
                    };
                    var userRole = await _roleService.FirstOrDefaultAsync(r =>
                        r.Name == (RoleName) Enum.Parse(typeof(RoleName), info.Role, true));
                    user.Role = userRole;

                    await _userService.CreateAsync(user);

                    return Redirect(Url.Action("GetAll", "User"));
                }

                ModelState.AddModelError("", "Пользователь существует!");
                return View("CreatePage");
            }

            ModelState.AddModelError("", "Номер неправильного формата!");
            return View("CreatePage");
        }

        public IActionResult CreatePage()
        {
            return View();
        }

        public async Task<IActionResult> DeleteUser(UserInfo info)
        {
            await _userService.DeleteAsync(info.Id);

            return RedirectToAction("GetAll", "User");
        }
    }
}
