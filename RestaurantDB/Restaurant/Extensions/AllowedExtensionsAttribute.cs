﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using Microsoft.AspNetCore.Http;

namespace Restaurant.Extensions;

public class AllowedExtensionsAttribute : ValidationAttribute
{
    private readonly List<string> _extensions;
    public AllowedExtensionsAttribute(string[] extensions)
    {
        _extensions = new List<string>(extensions);
    }

    protected override ValidationResult IsValid(
        object value, ValidationContext validationContext)
    {
        var file = value as IFormFile;
        if (file != null)
        {
            var extension = Path.GetExtension(file.FileName);
            if (!_extensions.Contains(extension.ToLower()))
            {
                return new ValidationResult(GetErrorMessage());
            }
        }

        return ValidationResult.Success;
    }

    public string GetErrorMessage()
    {
        return $"Это расширение фото не поддерживается!";
    }
}