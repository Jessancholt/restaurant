﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Restaurant.Models.Account
{
    public class RegisterModel
    {
        [Required(ErrorMessage = "Не указан логин")]
        [Display(Name = "Логин:")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Не указан пароль")]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль:")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Пароль введен неверно")]
        [Display(Name = "Подтвердите пароль:")]
        public string ConfirmPassword { get; set; }
        [Display(Name = "Имя:")]
        [Required(ErrorMessage = "Не указано имя")]
        public string FirstName { get; set; }
        [Display(Name = "Фамилия:")]
        [Required(ErrorMessage = "Не указана фамилия")]
        public string LastName { get; set; }

        [Display(Name = "Номер телефона:")]
        [Required(ErrorMessage = "Не указан номер телефона")]
        [DataType(DataType.PhoneNumber, ErrorMessage = "Неверный формат номера")]
        public string PhoneNumber { get; set; }
        [Display(Name = "Роль:")]
        public string Role { get; set; }
    }
}
