﻿using RestaurantDB.Model;
using System.Collections.Generic;
using System.Linq;

namespace Restaurant.Models.Home
{
    public class Cart
    {
        private List<CartLineInfo> lineCollection = new List<CartLineInfo>();
        public void AddItem(Dessert dish, int quantity)
        {
            CartLineInfo line = lineCollection
                .Where(g => g.DishId == dish.Id)
                .FirstOrDefault();

            if (line == null)
            {
                lineCollection.Add(new CartLineInfo
                {
                    DishId = dish.Id,
                    Quantity = quantity,
                    Price = dish.Price,
                    Title = dish.Title
                });
            }
            else
            {
                line.Quantity += quantity;
            }
        }

        public void RemoveLine(Dessert dish)
        {
            lineCollection.RemoveAll(l => l.DishId == dish.Id);
        }

        public decimal ComputeTotalValue()
        {
            return lineCollection.Sum(e => e.Price * e.Quantity);
        }
        public void Clear()
        {
            lineCollection.Clear();
        }

        public IEnumerable<CartLineInfo> Lines
        {
            get { return lineCollection; } set { lineCollection = value.ToList(); }
        }
    }
}
