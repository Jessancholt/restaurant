﻿using RestaurantDB.Model;

namespace Restaurant.Models.Home
{
    public class CartLineInfo
    {
        public int DishId { get; set; }
        public decimal Price { get; set; }
        public string Title { get; set; }
        public int Quantity { get; set; }
    }
}
