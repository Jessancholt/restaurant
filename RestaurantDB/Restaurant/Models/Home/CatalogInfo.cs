﻿using RestaurantDB.Model;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Restaurant.Models.Home
{
    public class CatalogInfo
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Поле название обязательно к заполнению")]
        public string Title { get; set; }
        public IEnumerable<Dessert> Dishes { get; set; }
    }
}
