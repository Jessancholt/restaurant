﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Restaurant.Extensions;

namespace Restaurant.Models.Home
{
    public class DishInfo
    {
        public int Id { get; set; }

        public int MenuId { get; set; }
        [Required(ErrorMessage = "Название блюда обязательно для ввода")]
        public string Title { get; set; }
        public string Description { get; set; }
        [Required(ErrorMessage = "Вес обязателен для ввода")]
        public int Weight { get; set; }
        public int Rating { get; set; }
        public double Rate { get; set; }
        [Column(TypeName = "money")]
        [Required(ErrorMessage = "Цена обязательна для ввода")]
        public decimal Price { get; set; }
        public TimeSpan DeliveryTime { get; set; }

        [Required(ErrorMessage = "Картинка обязательна для ввода")]
        [Display(Name = "File")]
        [AllowedExtensions(new string[] { ".jpg", ".png" })]
        public IFormFile Image { get; set; }
        public byte[] Avatar { get; set; }
    }
}
