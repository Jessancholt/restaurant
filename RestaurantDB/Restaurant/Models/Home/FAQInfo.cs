﻿namespace Restaurant.Models.Home
{
    public class FAQInfo
    {
        public int Id { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
    }
}
