﻿using RestaurantDB.Model;
using System.Collections.Generic;

namespace Restaurant.Models.Home
{
    public class OrderInfo
    {
        public int Id { get; set; }
        public string Status { get; set; }
        public List<CartLine> Dishes { get; set; }
    }
}
