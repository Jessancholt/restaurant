﻿using RestaurantDB.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Restaurant.Models.Home
{
    public class UserInfo
    {
        public int Id { get; set; }
        [Display(Name = "Имя:")]
        [Required(ErrorMessage = "Не указано имя")]
        public string FirstName { get; set; }
        [Display(Name = "Фамилия:")]
        [Required(ErrorMessage = "Не указана фамилия")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Не указан логин")]
        [Display(Name = "Логин:")]
        public string Login { get; set; }
        [Required(ErrorMessage = "Не указан пароль")]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль:")]
        public string Password { get; set; }
        [Display(Name = "Номер телефона:")]
        [Required(ErrorMessage = "Не указан номер телефона")]
        [DataType(DataType.PhoneNumber, ErrorMessage = "Неверный формат номера")]
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        [Display(Name = "Роль:")]
        public string Role { get; set; }
        public IEnumerable<Dessert> Dishes { get; set; }
        public IEnumerable<Rating> DishRatings { get; set; }
    }
}
