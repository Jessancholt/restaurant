﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Restaurant.Models.ViewModels
{
    public class FilterViewModel
    {
        public FilterViewModel(string title)
        {
            // устанавливаем начальный элемент, который позволит выбрать всех
            SelectedTitle = title;
        }
        public FilterViewModel(string firstName, string lastName)
        {
            // устанавливаем начальный элемент, который позволит выбрать всех
            SelectedFirstName = firstName;
            SelectedLastName = lastName;
        }
        public string SelectedTitle { get; private set; } // введенный заголовок
        public string SelectedFirstName { get; private set; }
        public string SelectedLastName { get; private set; }
    }
}
