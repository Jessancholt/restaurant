﻿using Restaurant.Models.Home;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Restaurant.Models.ViewModels
{
    public class IndexViewModel
    {
        public IEnumerable<UserInfo> Users { get; set; }
        public IEnumerable<DishInfo> Dishes { get; set; }
        public IEnumerable<CatalogInfo> Menus { get; set; }
        public IEnumerable<OrderInfo> Orders { get; set; }
        public IEnumerable<FAQInfo> Faqs { get; set; }
        public int MenuId { get; set; }
        public FilterViewModel FilterViewModel { get; set; }
        public PageViewModel PageViewModel { get; set; }
        public SortViewModel SortViewModel { get; set; }
    }
}
