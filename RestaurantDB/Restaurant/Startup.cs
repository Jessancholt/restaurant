using CandystoreDB.Finders;
using CandystoreDB.Finders.Interfaces;
using CandystoreDB.Models;
using CommonDataAccess.Repository;
using CommonDataAccess.Repository.Interfaces;
using CommonDataAccess.UnitOfWork;
using CommonDataAccess.UnitOfWork.Interfaces;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RestaurantBL.Services;
using RestaurantBL.Services.Interfaces;
using RestaurantDB;
using RestaurantDB.Model;

namespace Restaurant
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews().AddNewtonsoftJson(x => x.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            services.AddScoped<DbContext, RestaurantDBContext>();
            services.AddDbContext<RestaurantDBContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("MSSQL"));
                options.UseLazyLoadingProxies();
            });

            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IService<User>, Service<User>>();
            services.AddScoped<IService<Dessert>, Service<Dessert>>();
            services.AddScoped<IService<Catalog>, Service<Catalog>>();
            services.AddScoped<IService<Order>, Service<Order>>();
            services.AddScoped<IService<CartLine>, Service<CartLine>>();
            services.AddScoped<IService<FAQ>, Service<FAQ>>();
            services.AddScoped<IService<Comment>, Service<Comment>>();
            services.AddScoped<IService<Address>, Service<Address>>();
            services.AddScoped<IService<Role>, Service<Role>>();
            services.AddScoped<IService<Rating>, Service<Rating>>();

            services.AddScoped<ICommonFinder<CartLine>, CommonFinder<CartLine>>();
            services.AddScoped<ICommonFinder<Catalog>, CommonFinder<Catalog>>();
            services.AddScoped<ICommonFinder<Comment>, CommonFinder<Comment>>();
            services.AddScoped<ICommonFinder<FAQ>, CommonFinder<FAQ>>();
            services.AddScoped<ICommonFinder<Order>, CommonFinder<Order>>();
            services.AddScoped<ICommonFinder<Dessert>, CommonFinder<Dessert>>();
            services.AddScoped<ICommonFinder<User>, CommonFinder<User>>();
            services.AddScoped<ICommonFinder<Address>, CommonFinder<Address>>();
            services.AddScoped<ICommonFinder<Role>, CommonFinder<Role>>();
            services.AddScoped<ICommonFinder<Rating>, CommonFinder<Rating>>();

            services.AddScoped<IRepository<CartLine>, Repository<CartLine>>();
            services.AddScoped<IRepository<Catalog>, Repository<Catalog>>();
            services.AddScoped<IRepository<Comment>, Repository<Comment>>();
            services.AddScoped<IRepository<FAQ>, Repository<FAQ>>();
            services.AddScoped<IRepository<Order>, Repository<Order>>();
            services.AddScoped<IRepository<Dessert>, Repository<Dessert>>();
            services.AddScoped<IRepository<User>, Repository<User>>();
            services.AddScoped<IRepository<Address>, Repository<Address>>();
            services.AddScoped<IRepository<Role>, Repository<Role>>();
            services.AddScoped<IRepository<Rating>, Repository<Rating>>();

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                    .AddCookie(options => //CookieAuthenticationOptions
                    {
                        options.LoginPath = new Microsoft.AspNetCore.Http.PathString("/Account/Login");
                    });
            services.AddDistributedMemoryCache();
            services.AddSession();
        }
        
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseSession();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Catalog}/{action=GetAll}/{id?}");
            });
        }
    }
}
