﻿using System.Linq.Expressions;

namespace RestaurantBL.Services.Interfaces
{
    public interface IService<T>
        where T : class
    {
        public Task<List<T>> GetAsync();

        public Task<List<T>> GetAsync(Expression<Func<T, bool>> condition);

        public Task<T> GetAsync(int id);

        public Task<bool> ExistsAsync(Expression<Func<T, bool>> condition);

        public Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> condition);

        public Task CreateAsync(T entity);

        public Task DeleteAsync(int id);

        public Task UpdateAsync(T entity);
    }
}