﻿using System.Linq.Expressions;
using CandystoreDB.Finders.Interfaces;
using CommonDataAccess.Repository.Interfaces;
using CommonDataAccess.UnitOfWork.Interfaces;
using RestaurantBL.Services.Interfaces;

namespace RestaurantBL.Services
{
    public class Service<T> : IService<T>
        where T : class
    {
        private readonly ICommonFinder<T> _finder;
        private readonly IRepository<T> _repository;
        private readonly IUnitOfWork _uow;

        public Service(ICommonFinder<T> finder,
            IRepository<T> repository,
            IUnitOfWork uow)
        {
            _finder = finder;
            _repository = repository;
            _uow = uow;
        }

        public Task<List<T>> GetAsync()
        {
            return _finder.GetAsync();
        }

        public Task<List<T>> GetAsync(Expression<Func<T, bool>> condition)
        {
            return _finder.GetAsync(condition);
        }

        public Task<T> GetAsync(int id)
        {
            return _finder.GetAsync(id);
        }

        public Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> condition)
        {
            return _finder.FirstOrDefaultAsync(condition);
        }

        public Task<bool> ExistsAsync(Expression<Func<T, bool>> condition)
        {
            return _finder.ExistsAsync(condition);
        }

        public async Task CreateAsync(T entity)
        {
            await _repository.CreateAsync(entity);
            await _uow.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            var result = await _finder.GetAsync(id);
            if (result != null)
                _repository.DeleteAsync(result);

            await _uow.SaveChangesAsync();
        }

        public Task UpdateAsync(T entity)
        {
            _repository.UpdateAsync(entity);
            return _uow.SaveChangesAsync();
        }
    }
}