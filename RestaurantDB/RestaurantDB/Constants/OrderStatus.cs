﻿namespace RestaurantDB.Constants
{
    public enum OrderStatus
    {
        Active,
        Done,
        Cancelled
    }
}
