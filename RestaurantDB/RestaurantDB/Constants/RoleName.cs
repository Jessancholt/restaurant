﻿namespace RestaurantDB.Constants
{
    public enum RoleName
    {
        Admin = 1,
        User = 2,
        Cook = 3,
    }
}
