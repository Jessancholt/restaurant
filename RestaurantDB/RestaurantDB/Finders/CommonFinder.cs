﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CandystoreDB.Finders.Interfaces;
using CommonDataAccess.Finder;
using CommonDataAccess.Finder.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace CandystoreDB.Finders
{
    public class CommonFinder<T> : ICommonFinder<T>
        where T : class
    {
        private readonly IFinder<T> _finder;

        public CommonFinder(DbContext context)
        {
            _finder = new Finder<T>(context);
        }

        public Task<List<T>> GetAsync()
        {
            return _finder.Find().ToListAsync();
        }

        public Task<List<T>> GetAsync(Expression<Func<T, bool>> condition)
        {
            return _finder.Find().Where(condition).ToListAsync();
        }

        public Task<T> GetAsync(int id)
        {
            return _finder.Find(id);
        }

        public Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> condition)
        {
            return _finder.Find().FirstOrDefaultAsync(condition);
        }

        public Task<bool> ExistsAsync(Expression<Func<T, bool>> condition)
        {
            return _finder.ExistsAsync(condition);
        }
    }
}