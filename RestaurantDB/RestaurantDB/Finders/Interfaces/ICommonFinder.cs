﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CandystoreDB.Finders.Interfaces
{
    public interface ICommonFinder<T>
    {
        public Task<List<T>> GetAsync();

        public Task<List<T>> GetAsync(Expression<Func<T, bool>> condition);

        public Task<T> GetAsync(int id);

        public Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> condition);

        public Task<bool> ExistsAsync(Expression<Func<T, bool>> condition);
    }
}