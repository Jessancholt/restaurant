﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace RestaurantDB.Migrations
{
    public partial class ScriptsAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var path = Environment.CurrentDirectory;
            var roles = System.IO.File.ReadAllText(path + "/Scripts/Roles.sql");
            migrationBuilder.Sql(roles);

            var users = System.IO.File.ReadAllText(path + "/Scripts/Users.sql");
            migrationBuilder.Sql(users);

            var catalogs = System.IO.File.ReadAllText(path + "/Scripts/Catalogs.sql");
            migrationBuilder.Sql(catalogs);

            var desserts = System.IO.File.ReadAllText(path + "/Scripts/Desserts.sql");
            migrationBuilder.Sql(desserts);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}