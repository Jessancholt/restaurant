﻿namespace RestaurantDB.Model
{
    public class CartLine
    {
        public int Id { get; set; }
        public int Quantity { get; set; }
        public int DessertId { get; set; }
        public virtual Dessert Dessert { get; set; }
        public int OrderId { get; set; }
        public virtual Order Order { get; set; }
    }
}
