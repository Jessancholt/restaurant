﻿using System;
using System.Collections.Generic;
using RestaurantDB.Model;

namespace CandystoreDB.Models
{
    public class Comment
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public int Rating { get; set; }
        public DateTimeOffset PublicationDate { get; set; }
        public virtual User User { get; set; }
        public virtual Order Order { get; set; }
    }
}