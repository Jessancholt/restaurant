﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net.Mime;
using System.Text;

namespace RestaurantDB.Model
{
    public class Dessert
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public int Weight { get; set; }        
        [Column(TypeName = "money")]
        public decimal Price { get; set; }
        public byte[] Image { get; set; }

        public double Rate => Rates.Any() ? Rates.Average(r => r.Rate) : 0;
        public virtual CartLine CartLine { get; set; }
        public virtual ICollection<Rating> Rates { get; set; } = new HashSet<Rating>();

        public virtual ICollection<Order> Orders { get; set; } = new HashSet<Order>();
        public virtual ICollection<User> Users { get; set; } = new HashSet<User>();
    }
}
