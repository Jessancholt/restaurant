﻿using System.Collections.Generic;

namespace RestaurantDB.Model
{
    public class FAQ
    {
        public int Id { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public virtual ICollection<User> Users { get; set; } = new HashSet<User>();
    }
}