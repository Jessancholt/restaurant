﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using CandystoreDB.Models;
using RestaurantDB.Constants;

namespace RestaurantDB.Model
{
    public class Order
    {
        public int Id { get; set; }
        public OrderStatus Status{ get; set; }
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset EndDate { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public int? CommentId { get; set; }
        public virtual Comment? Comment { get; set; }
        public virtual ICollection<CartLine> CartLines { get; set; } = new HashSet<CartLine>();
        
    }
}
