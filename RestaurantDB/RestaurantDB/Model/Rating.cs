﻿namespace RestaurantDB.Model
{
    public class Rating
    {
        public int Id { get; set; }
        public int Rate { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public int DishId { get; set; } 
        public virtual Dessert Dish { get; set; }
    }
}
