﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using RestaurantDB.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using CandystoreDB.Models;

namespace RestaurantDB
{
    public class RestaurantDBContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Catalog> Catalogs { get; set; }
        public DbSet<Dessert> Desserts { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<FAQ> Faqs { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Rating> Ratings { get; set; }
        public DbSet<CartLine> CartLines { get; set; }

        public RestaurantDBContext(DbContextOptions<RestaurantDBContext> options) : base(options)
        {
        }
    }
}
