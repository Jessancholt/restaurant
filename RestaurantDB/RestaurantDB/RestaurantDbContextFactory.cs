﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace RestaurantDB
{
    public class RestaurantDbContextFactory : IDesignTimeDbContextFactory<RestaurantDBContext>
    {
        public RestaurantDBContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<RestaurantDBContext>();
            optionsBuilder.UseSqlServer(
                "Data Source=DESKTOP-TL46QKF\\JESSANCHOLTSQL;Initial Catalog=RestaurantDB;Integrated Security=True;Connect Timeout=60;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            return new RestaurantDBContext(optionsBuilder.Options);
        }
    }
}