USE [RestaurantDB]
GO
SET IDENTITY_INSERT [dbo].[Catalogs] ON 
GO
INSERT [dbo].[Catalogs] ([Id], [Title]) VALUES (1, N'Выпечка')
GO
INSERT [dbo].[Catalogs] ([Id], [Title]) VALUES (2, N'Торты')
GO
INSERT [dbo].[Catalogs] ([Id], [Title]) VALUES (3, N'Тарталетки')
GO
INSERT [dbo].[Catalogs] ([Id], [Title]) VALUES (4, N'Пирожные')
GO
INSERT [dbo].[Catalogs] ([Id], [Title]) VALUES (5, N'Эклеры')
GO
SET IDENTITY_INSERT [dbo].[Catalogs] OFF
GO
