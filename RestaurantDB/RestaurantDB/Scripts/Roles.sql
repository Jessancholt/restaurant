USE [RestaurantDB]
GO
SET IDENTITY_INSERT [dbo].[Roles] ON 
GO
INSERT [dbo].[Roles] ([Id], [Name]) VALUES (1, 1)
GO
INSERT [dbo].[Roles] ([Id], [Name]) VALUES (2, 2)
GO
INSERT [dbo].[Roles] ([Id], [Name]) VALUES (3, 3)
GO
SET IDENTITY_INSERT [dbo].[Roles] OFF
GO
