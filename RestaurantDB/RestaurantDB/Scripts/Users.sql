USE [RestaurantDB]
GO
SET IDENTITY_INSERT [dbo].[Users] ON 
GO
INSERT [dbo].[Users] ([Id], [FirstName], [LastName], [Login], [Password], [PhoneNumber], [RoleId]) VALUES (1, N'admin', N'admin', N'admin', N'admin', '+375333333333', 1)
GO
INSERT [dbo].[Users] ([Id], [FirstName], [LastName], [Login], [Password], [PhoneNumber], [RoleId]) VALUES (2, N'cook', N'cook', N'cook', N'cook', '+375334444444', 3)
GO
SET IDENTITY_INSERT [dbo].[Users] OFF
GO
